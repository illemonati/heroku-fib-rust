use actix_web::{get, web, App, HttpServer, Responder};
use std::env;

mod fib;







#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let port = env::var("PORT").unwrap_or(String::from("8080"));

    HttpServer::new(|| App::new()
        .service(fib::fib_service)
    )
        .bind(format!("0.0.0.0:{}", port))?
        .run()
        .await
}
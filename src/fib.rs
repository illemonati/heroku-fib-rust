use serde::{Deserialize, Serialize};
use actix_web::{get, web, App, HttpServer, Responder, HttpResponse, Result};

#[derive(Serialize, Deserialize)]
struct FibRes {
    res: usize,
}

#[get("/fib/{n}")]
pub async fn fib_service(info: web::Path<(usize,)>) -> Result<HttpResponse> {
    let n = info.0;

    Ok(HttpResponse::Ok().json(FibRes {
        res: fib(n).await,
    }))
}

async fn fib(n: usize) -> usize {
    let mut a = 0;
    let mut b = 1;
    for _ in 0..n {
        let temp = a;
        a = b;
        b += temp;
    }
    a
}